const UserData = [
  {
    id: "1",
    name: "Ram Kumar",
    userid: "RK"
  },
  {
    id: "2",
    name: "Rahul Bara",
    userid: "RB"
  },
  {
    id: "3",
    name: "Shyam Kumar",
    userid: "SK"
  },
  {
    id: "4",
    name: "Pawan Kumar",
    userid: "PK"
  },
  {
    id: "5",
    name: "Sandeep Sharma",
    userid: "SS"
  },
  {
    id: "6",
    name: "Ram Raj",
    userid: "RR"
  },
  {
    id: "7",
    name: "Ramneek Jassar",
    userid: "RJ"
  }
];

export { UserData };
