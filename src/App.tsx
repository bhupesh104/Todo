import React, { Component } from "react";
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import logger from "redux-logger";
import { persistReducer, persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";
import thunk from "redux-thunk";
import TodoList from "./components/Todo/TodoList";
import { persistConfig, rootReducer } from "./reducers/index";

export default class App extends Component<{}> {
  public render() {
    const persist = persistReducer(persistConfig, rootReducer);
    const store = createStore(persist, applyMiddleware(thunk, logger));
    const persistor = persistStore(store);
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <TodoList />
        </PersistGate>
      </Provider>
    );
  }
}
