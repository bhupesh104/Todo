import { Reducer } from "redux";
import {
  AddUpdateTodoAction,
  AddUpdateUserAction,
  RemoveTodoAction,
  ResetTodoAction,
  Todo,
  TodoActionTypes
} from "../actions/todo/todoList";

import { v4 } from "uuid";
import {
  AddTodoViewAction,
  RemoveTodoViewAction,
  ResetTodoViewAction,
  TodoView,
  TodoViewActionTypes
} from "../actions/todo/todoView";

import {
  DismissTodoUserAction,
  ShowTodoUserAction,
  TodoUser,
  TodoUserActionTypes
} from "../actions/todo/todoUser";

export interface TodoState {
  list: Record<string, Todo>;
  view: TodoView[];
  user: TodoUser;
}

const initialTodoViewState: TodoView[] = [
  { id: v4(), orderID: 1 },
  { id: v4(), orderID: 2 },
  { id: v4(), orderID: 3 },
  { id: v4(), orderID: 4 }
];

export const initialTodoState: TodoState = {
  list: {},
  view: initialTodoViewState,
  user: {
    showOverlay: false,
    todoId: ""
  }
};

export const todoReducer: Reducer<TodoState> = (
  state: TodoState = initialTodoState,
  action:
    | AddUpdateTodoAction
    | AddUpdateUserAction
    | RemoveTodoAction
    | ResetTodoAction
    | AddTodoViewAction
    | RemoveTodoViewAction
    | ResetTodoViewAction
    | DismissTodoUserAction
    | ShowTodoUserAction
) => {
  switch (action.type) {
    case TodoActionTypes.Add_Update:
      return {
        // current: state.current
        list: { ...state.list, [action.id]: action.todo },
        view: [...state.view],
        user: { ...state.user }
      };
    case TodoActionTypes.Add_Update_User:
      return {
        // current: state.current
        list: {
          ...state.list,
          [action.id]: { ...state.list[action.id], userId: action.userId }
        },
        view: [...state.view],
        user: {
          showOverlay: false,
          todoId: ""
        }
      };
    case TodoActionTypes.Reset:
      return {
        list: {},
        view: initialTodoViewState,
        user: {
          showOverlay: false,
          todoId: ""
        }
      };
    case TodoActionTypes.Remove:
      return state;
    case TodoViewActionTypes.Add:
      return {
        // current: state.current
        list: { ...state.list },
        view: [...state.view, action.todoView],
        user: {
          showOverlay: false,
          todoId: ""
        }
      };
    case TodoViewActionTypes.Reset:
      return {
        list: { ...state.list },
        view: initialTodoViewState,
        user: {
          showOverlay: false,
          todoId: ""
        }
      };
    case TodoViewActionTypes.Remove:
      return state;
    case TodoUserActionTypes.show:
      return {
        // current: state.current
        list: { ...state.list },
        view: [...state.view],
        user: {
          showOverlay: true,
          todoId: action.todoId
        }
      };
    case TodoUserActionTypes.Dismiss:
      return {
        list: { ...state.list },
        view: [...state.view],
        user: {
          showOverlay: false,
          todoId: ""
        }
      };
    default:
      return state;
  }
};
