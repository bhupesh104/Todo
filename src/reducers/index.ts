import { AnyAction, combineReducers, Reducer } from "redux";
import storage from "redux-persist/lib/storage";
import { initialTodoState, todoReducer, TodoState } from "./Todo";

export interface AppState {
  todo: TodoState;
}

const initialState = {
  todo: initialTodoState
};

const defaultReducer = combineReducers<AppState>({
  todo: todoReducer
});

export const rootReducer: Reducer<AppState> = (
  state: AppState = initialState,
  action: AnyAction
) => {
  switch (action.type) {
    default:
      return defaultReducer(state, action);
  }
};

export const persistConfig = {
  key: "root",
  storage
};
