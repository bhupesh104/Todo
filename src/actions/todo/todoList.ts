import { Action, ActionCreator } from "redux";

export enum TodoActionTypes {
  Add_Update = "ADD_Update_TODO",
  Remove = "REMOVE_TODO",
  Reset = "RESET_TODO",
  Save = "SAVE_TODO",
  Add_Update_User = "ADD_UPDATE_USER"
}

export interface Todo {
  text: string;
  userId: string;
}

export interface AddUpdateTodoAction
  extends Action<TodoActionTypes.Add_Update> {
  id: string;
  todo: Todo;
}

export const addUpdate: ActionCreator<AddUpdateTodoAction> = (
  id: string,
  todo: Todo
) => ({
  type: TodoActionTypes.Add_Update,
  id,
  todo
});

export interface AddUpdateUserAction
  extends Action<TodoActionTypes.Add_Update_User> {
  id: string;
  userId: string;
}

export const addUpdateUser: ActionCreator<AddUpdateUserAction> = (
  id: string,
  userId: string
) => ({
  type: TodoActionTypes.Add_Update_User,
  id,
  userId
});

export interface RemoveTodoAction extends Action<TodoActionTypes.Remove> {
  id: string;
}

export const remove: ActionCreator<RemoveTodoAction> = (id: string) => ({
  type: TodoActionTypes.Remove,
  id
});

export interface ResetTodoAction extends Action<TodoActionTypes.Reset> {}

export const reset: ActionCreator<ResetTodoAction> = () => ({
  type: TodoActionTypes.Reset
});

export interface SaveTodoAction extends Action<TodoActionTypes.Save> {
  todoData: Record<string, Todo>;
}

export const save: ActionCreator<SaveTodoAction> = (
  todoData: Record<string, Todo>
) => ({
  type: TodoActionTypes.Save,
  todoData
});
