import { Action, ActionCreator } from "redux";

export enum TodoUserActionTypes {
  show = "SHOW_USER_OVERLAY",
  select = "SELECT_USER_OVERLAY",
  Dismiss = "DISMISS_USER_OVERLAY"
}
export interface TodoUser {
  showOverlay: boolean;
  todoId: string;
}

export interface ShowTodoUserAction extends Action<TodoUserActionTypes.show> {
  todoId: string;
}

export const showUserView: ActionCreator<ShowTodoUserAction> = (
  todoId: string
) => ({
  type: TodoUserActionTypes.show,
  todoId
});

export interface DismissTodoUserAction
  extends Action<TodoUserActionTypes.Dismiss> {}

export const dismissUserView: ActionCreator<DismissTodoUserAction> = () => ({
  type: TodoUserActionTypes.Dismiss
});

export interface SelectTodoUserAction
  extends Action<TodoUserActionTypes.select> {
  userId: string;
  todoId: string;
}

export const selectTodoUser: ActionCreator<SelectTodoUserAction> = (
  userId: string,
  todoId: string
) => ({
  type: TodoUserActionTypes.select,
  userId,
  todoId
});
