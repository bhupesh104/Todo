import { Action, ActionCreator } from "redux";

export enum TodoViewActionTypes {
  Add = "ADD_TODO_VIEW",
  Remove = "REMOVE_TODO_VIEW",
  Reset = "RESET_TODO_VIEW"
}

export interface TodoView {
  id: string;
  orderID: number;
}

export interface AddTodoViewAction extends Action<TodoViewActionTypes.Add> {
  todoView: TodoView;
}

export const addView: ActionCreator<AddTodoViewAction> = (
  todoView: TodoView
) => ({
  type: TodoViewActionTypes.Add,
  todoView
});

export interface RemoveTodoViewAction
  extends Action<TodoViewActionTypes.Remove> {
  id: string;
}

export const removeView: ActionCreator<RemoveTodoViewAction> = (
  id: string
) => ({
  type: TodoViewActionTypes.Remove,
  id
});

export interface ResetTodoViewAction
  extends Action<TodoViewActionTypes.Reset> {}

export const resetView: ActionCreator<ResetTodoViewAction> = () => ({
  type: TodoViewActionTypes.Reset
});
