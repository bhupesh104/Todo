import React, { PureComponent } from "react";
import { AsyncStorage, StyleSheet, Text, View } from "react-native";
import { Header, Icon } from "react-native-elements";
import { THEME_COLORS } from "../../common/colors";

interface Props {
  title: string;
  subtitle: string;
}
export default class TodoHeader extends PureComponent<Props> {
  public renderLeftComponent() {
    return <Icon name="arrow-back" color={THEME_COLORS.iconBackground} />;
  }
  public render() {
    return (
      <Header
        leftComponent={this.renderLeftComponent()}
        centerComponent={this.renderCenterComponent()}
        rightComponent={this.renderRightComponent()}
        containerStyle={styles.headerContainer}
      />
    );
  }
  private renderCenterComponent() {
    return (
      <View>
        <Text style={styles.title}>{this.props.title}</Text>
        <Text style={styles.subtitle}>{this.props.subtitle}</Text>
      </View>
    );
  }
  private renderRightComponent() {
    return (
      <Icon
        name="save"
        color={THEME_COLORS.iconBackground}
        onPress={this.onPressSave}
      />
    );
  }
  private onPressSave = (): void => {
    AsyncStorage.clear();
  };
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: THEME_COLORS.darkBackground
  },
  textContainers: {
    flex: 1,
    paddingTop: 45,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    flex: 1,
    color: "white",
    fontWeight: "bold",
    textAlign: "left",
    fontSize: 18
  },
  subtitle: {
    flex: 1,
    textAlign: "center",
    color: "white"
  }
});
