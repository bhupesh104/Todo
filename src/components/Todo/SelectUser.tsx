import React, { Component } from "react";
import { FlatList, StyleSheet, Text, TouchableOpacity } from "react-native";
import { Avatar, Overlay } from "react-native-elements";
import { connect } from "react-redux";
import { dismissUserView, TodoUser } from "../../actions/todo/todoUser";
import { THEME_COLORS } from "../../common/colors";
interface Props {
  userData: any;
  TodoSelectUser: TodoUser;
  dismissUserView: any;
  onTodoUserSelect: any;
}

class SelectUser extends Component<Props> {
  public render(): JSX.Element {
    return (
      <Overlay
        isVisible={this.props.TodoSelectUser.showOverlay}
        windowBackgroundColor="rgba(255, 255, 255, .5)"
        overlayBackgroundColor="white"
        width="auto"
        height="auto"
        onBackdropPress={this.onBackdropPress}
        containerStyle={styles.Container}
      >
        <FlatList
          data={this.props.userData}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          style={styles.flatList}
        />
      </Overlay>
    );
  }

  private onBackdropPress = (): void => {
    this.props.dismissUserView();
  };
  private renderItem = ({ item }: any) => {
    return (
      <TouchableOpacity
        style={styles.itemContainers}
        onPress={() =>
          this.props.onTodoUserSelect(
            this.props.TodoSelectUser.todoId,
            item.userid
          )
        }
      >
        <Avatar
          size="small"
          rounded={true}
          title={item.userid}
          titleStyle={styles.avatarTitle}
          containerStyle={styles.avatar}
        />
        <Text style={styles.name}>{item.name}</Text>
      </TouchableOpacity>
    );
  };
  private keyExtractor = (item: any) => {
    return item.id;
  };
  private onSelectUser = (item: any): void => {
    this.props.onTodoUserSelect(this.props.TodoSelectUser.todoId, item.userid);
  };
}
const mapStateToProps = (state: any) => {
  return {
    TodoSelectUser: state.todo.user
  };
};

export default connect(
  mapStateToProps,
  { dismissUserView }
)(SelectUser);

const styles = StyleSheet.create({
  Container: {
    justifyContent: "center",
    alignItems: "center"
  },
  itemContainers: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    height: 50
  },
  name: {
    color: THEME_COLORS.darkBackground,
    fontSize: 18
  },
  flatList: {
    maxHeight: 300
  },
  avatarTitle: {
    color: THEME_COLORS.darkBackground
  },
  avatar: {
    marginLeft: 20
  }
});
