import React, { Component } from "react";
import { StyleSheet, TextInput, View } from "react-native";
import { Avatar, CheckBox, Icon } from "react-native-elements";
import { THEME_COLORS } from "../../common/colors";

interface Props {
  uniqueId: string;
  text: string;
  placeHolderText: string;
  avatar: any;
  onPressSelectUser: any;
  onEndTextEditing(text: string, uniqueId: string): void;
}
interface State {
  text: string;
  checked: boolean;
}
export default class TodoItem extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      text: this.props.text,
      checked: false
    };
  }
  public render() {
    return (
      <View style={styles.containter}>
        <CheckBox
          checkedIcon="dot-circle-o"
          uncheckedIcon="circle-o"
          checkedColor={THEME_COLORS.darkBackground}
          uncheckedColor={THEME_COLORS.darkBackground}
          checked={this.state.checked}
          onPress={this.onClickCheckBox}
        />
        <TextInput
          style={styles.textInput}
          onChangeText={this.onChangeText}
          value={this.state.text}
          onEndEditing={this.onEndEdit}
          placeholder={this.props.placeHolderText}
          placeholderTextColor={THEME_COLORS.lightText}
          onBlur={() => console.log("onblur")}
        />
        {this.renderSelectAvatar()}
      </View>
    );
  }

  private onClickCheckBox = () => {
    this.setState({ checked: !this.state.checked });
  };
  private onEndEdit = () => {
    this.props.onEndTextEditing(this.state.text, this.props.uniqueId);
  };
  private onChangeText = (text: string) => {
    this.setState({ text });
  };
  private renderSelectAvatar = () => {
    if (this.props.avatar) {
      return (
        <Avatar
          rounded={true}
          title={this.props.avatar}
          size="small"
          onPress={this.onPressUserSelection}
        />
      );
    }
    return (
      <Icon
        name="chevron-down"
        type="octicon"
        onPress={this.onPressUserSelection}
      />
    );
  };
  private onPressUserSelection = () => {
    this.props.onPressSelectUser(this.props.uniqueId);
  };
}
const styles = StyleSheet.create({
  containter: {
    marginRight: 20,
    marginVertical: 10,
    flexDirection: "row",
    justifyContent: "center"
  },
  headerContainer: {
    backgroundColor: THEME_COLORS.darkBackground
  },
  textInput: {
    height: 40,
    borderBottomWidth: 1,
    paddingLeft: 10,
    marginRight: 10,
    flex: 1,
    borderBottomColor: THEME_COLORS.darkBackground
  }
});
