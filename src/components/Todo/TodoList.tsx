import React, { Component } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { v4 } from "uuid";
import { addUpdate, addUpdateUser } from "../../actions/todo/todoList";
import { showUserView } from "../../actions/todo/todoUser";
import { addView } from "../../actions/todo/todoView";
import { THEME_COLORS } from "../../common/colors";
import { STATIC_TEXT } from "../../common/staticText";
import { UserData } from "../../common/userDetails";
import SelectUser from "./SelectUser";
import TodoHeader from "./TodoHeader";
import TodoItem from "./TodoItem";

interface Props {
  TodoView: any;
  addUpdate: any;
  addView: any;
  TodoListData: any;
  addUpdateUser: any;
  TodoSelectUser: any;
  showUserView: any;
}
class TodoList extends Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  public render() {
    return (
      <View style={styles.container}>
        <TodoHeader
          title={STATIC_TEXT.weekendParty}
          subtitle={STATIC_TEXT.toDoList}
        />
        <Text style={styles.instruction}>{STATIC_TEXT.addChecklistItem}</Text>
        <ScrollView>
          {this.renderTodoList()}
          {this.renderAddItem()}
        </ScrollView>
        {this.showOverlay()}
      </View>
    );
  }
  private renderTodoList() {
    if (this.props.TodoView.length > 0) {
      return this.props.TodoView.map((item: any) => (
        <TodoItem
          key={item.id}
          uniqueId={item.id}
          onEndTextEditing={this.onCompleteEditingText}
          placeHolderText={STATIC_TEXT.listItem}
          text={this.updateText(item.id)}
          avatar={this.updateAvatar(item.id)}
          onPressSelectUser={this.props.showUserView}
        />
      ));
    }
  }
  private renderAddItem() {
    return (
      <TouchableOpacity
        style={styles.addItemContainer}
        onPress={this.addTodoListView}
      >
        <Icon
          name="plus-circle"
          type="font-awesome"
          color={THEME_COLORS.darkBackground}
        />
        <Text style={styles.addItemText}>{STATIC_TEXT.addMoreItems}</Text>
      </TouchableOpacity>
    );
  }
  private showOverlay() {
    return (
      <SelectUser
        userData={UserData}
        onTodoUserSelect={this.onTodoUserSelection}
      />
    );
  }
  private onCompleteEditingText = (inputText: string, id: string): void => {
    if (inputText !== "") {
      this.props.addUpdate(id, { text: inputText, userId: "" });
    }
  };

  private updateText = (itemId: string): string => {
    if (this.props.TodoListData[itemId]) {
      return this.props.TodoListData[itemId].text;
    }
    return "";
  };
  private updateAvatar = (itemId: string): string => {
    if (this.props.TodoListData[itemId]) {
      return this.props.TodoListData[itemId].userId;
    }
    return "";
  };
  private onTodoUserSelection = (todoId: string, userId: string): void => {
    this.props.addUpdateUser(todoId, userId);
  };

  private addTodoListView = (): void => {
    this.props.addView({ id: v4(), orderID: 5 });
  };
}
const mapStateToProps = (state: any) => {
  return {
    TodoListData: state.todo.list,
    TodoView: state.todo.view,
    TodoSelectUser: state.todo.user
  };
};

export default connect(
  mapStateToProps,
  { addUpdate, addView, addUpdateUser, showUserView }
)(TodoList);

const styles = StyleSheet.create({
  container: {
    backgroundColor: THEME_COLORS.lightBackground,
    flexDirection: "column",
    flex: 1
  },
  addItemContainer: {
    flexDirection: "row",
    margin: 20,
    alignItems: "center"
  },
  addItemText: {
    color: THEME_COLORS.actionText,
    textAlign: "center",
    marginLeft: 20,
    fontSize: 20
  },
  instruction: {
    marginTop: 15,
    marginLeft: 10,
    color: THEME_COLORS.lightText
  }
});
